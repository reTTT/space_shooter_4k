var data = [
	{
		"chance": 3,
		"entity": [
			{
				"kind" : "med",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},

			{
				"kind" : "small",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},
		],
	},
	{
		"chance": 4.5,
		"entity": [
			{
				"kind" : "med",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},

			{
				"kind" : "small",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},
		],
	},
	{
		"chance": 4.0,
		"entity": [
			{
				"kind" : "med",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},
		],
	},
	{
		"chance": 4.0,
		"entity": [
			{
				"kind" : "small",
				"inst": preload("res://prefab/meteor/meteor.xscn"),
			},
		],
	},
]

func get_data():
	return data
