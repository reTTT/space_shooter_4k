
var data = [
	{
		"info": { 
			"name": "no name",
		},

		"time": 0,
		"layers": [
			{
				"name": "meteor",
				"delay": [0.25, 0.75],
				"groups": [
					{
						"chance": 10,
						"entity": [
							{
								"kind" : "big",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "med",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "small",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
				],
			},
		],
	},
]


func get_data():
	return data
