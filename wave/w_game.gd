
var data = [
	{
		"info": { 
			"name": "ID_WAVE_01",
		},
		"time": 60,
		"layers": [
			{
				"name": "meteor",
				"delay": [0.5, 1],
				"groups": [
					{
						"chance": 10,
						"entity": [
							{
								"kind" : "big",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "med",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "small",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "bonus",
				"delay": [6, 8.5],
				"groups": [
					{
						"chance": 5,
						"entity": [
							{
								"name": "health",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 2,
						"entity": [
							{
								"name": "immortal",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 7,
						"entity": [
							{
								"name": "fire_rate",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 2,
						"entity": [
							{
								"name": "rocket_green",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "enemy",
				"delay": [2.0, 3.5],
				"groups": [
					{
						"chance": 80,
						"entity": [
							{
								"path": -1,
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					
					{
						"chance": 66.6,
						"entity": [
							{
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					{
						"chance": 22.2,
						"entity": [
							{
								"kind": "medium",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					{
						"chance": 11.1,
						"entity": [
							{
								"kind": "hard",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					{
						"chance": 10.5,
						"spawn":{
							"col": 2,
							"row": 2,
							"offset": [128, 128],
							"inst": preload("res://prefab/formation/square.xscn"),
						},
						"entity": [
							{
								"count": 4,
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
				],
			},
		],
	},

	{
		"info": { 
			"name": "ID_WAVE_02",
		},
		"time": 90,
		"layers": [
			{
				"name": "meteor",
				"delay": [0.45, 0.85],
				"groups": [
					{
						"chance": 10,
						"entity": [
							{
								"kind" : "big",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "med",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "small",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "bonus",
				"delay": [5, 7],
				"groups": [
					{
						"chance": 9,
						"entity": [
							{
								"name": "health",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 7,
						"entity": [
							{
								"name": "immortal",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 15,
						"entity": [
							{
								"name": "fire_rate",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 8,
						"entity": [
							{
								"name": "rocket_green",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 20,
						"count": 1,
						"entity": [
							{
								"name": "upgrade_gun",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "enemy",
				"delay": [1.0, 2.5],
				"groups": [
					{
						"chance": 3,
						"entity": [
							{
								"path": -1,
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					
					{
						"chance": 2.5,
						"entity": [
							{
								"path": -1,
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
					
					{
						"chance": 1.5,
						"entity": [
							{
								"count": 2,
								"path": -1,
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					
					{
						"chance": 2,
						"entity": [
							{
								"count": 2,
								"path": -1,
								"kind": "easy",
								"weapon": "turret_1",
								"ai": "move_agressive", 
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
					
					{
						"chance": 3,
						"entity": [
							{
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					{
						"chance": 2.5,
						"entity": [
							{
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
					{
						"chance": 1.5,
						"entity": [
							{
								"count": 2,
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
							{
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
				],
			},
		],
	},

	{
		"info": { 
			"name": "ID_WAVE_03",
		},
		"time": 0,
		"layers": [
			{
				"name": "meteor",
				"delay": [0.45, 0.85],
				"groups": [
					{
						"chance": 10,
						"entity": [
							{
								"kind" : "big",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "med",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
					{
						"chance": 30,
						"entity": [
							{
								"kind" : "small",
								"inst": preload("res://prefab/meteor/meteor.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "bonus",
				"delay": [3, 6],
				"groups": [
					{
						"chance": 15,
						"entity": [
							{
								"name": "health",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 10,
						"entity": [
							{
								"name": "immortal",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 25,
						"entity": [
							{
								"name": "fire_rate",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 8,
						"entity": [
							{
								"name": "rocket_red",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 8,
						"entity": [
							{
								"name": "rocket_green",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
					{
						"chance": 20,
						"count": 1,
						"entity": [
							{
								"name": "upgrade_gun",
								"inst": preload("res://prefab/bonus/bonus_ex.xscn"),
							},
						],
					},
				],
			},
			{
				"name": "enemy",
				"delay": [0.75, 1.5],
				"groups": [
					{
						"chance": 2,
						"entity": [
							{
								"path": -1,
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					
					{
						"chance": 5,
						"entity": [
							{
								"path": -1,
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
					
					{
						"chance": 4.0,
						"entity": [
							{
								"path": -1,
								"inst": preload("res://prefab/enemy/enemy_03.xscn")
							},
						],
					},
					
					{
						"chance": 3,
						"entity": [
							{
								"inst": preload("res://prefab/enemy/enemy_01.xscn"),
							},
						],
					},
					{
						"chance": 4.5,
						"entity": [
							{
								"inst": preload("res://prefab/enemy/enemy_02.xscn"),
							},
						],
					},
					{
						"chance": 4.0,
						"entity": [
							{
								"inst": preload("res://prefab/enemy/enemy_03.xscn")
							},
						],
					},
				],
			},
		],
	},
]


func get_data():
	return data
