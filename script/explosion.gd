extends Node2D

var expl_shader_inst = preload("res://prefab/explosion/expl_shader.tscn")

var gfx
var sfx
var sfx_id

const SHADER = "../../expl_shader/world"
export (int, "L_METEOR", "L_BULLET", "L_ENEMY", "L_PLAYER", "L_EXPLOSION") var layer = 0

func _process(dt):
	if !sfx.is_voice_active(sfx_id):
		queue_free()

func _ready():
	if has_node(SHADER):
		var e = expl_shader_inst.instance()
		e.set_pos(get_pos())
		var n = get_node(SHADER)
		n.add_child(e)

	gfx = get_node("gfx")
	gfx.set_emitting(true)
	sfx = get_node("sfx")
	sfx_id = sfx.play("explosion")
	set_process(true)
