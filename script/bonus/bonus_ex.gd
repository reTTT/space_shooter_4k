extends "res://script/bonus/bonus.gd"

var bonusName = null
var cfg = null

func config(info):
	bonusName = get_val(info, "name", null)
	if !bonusName:
		return

	cfg = get_node("/root/db").get_bonus(bonusName)
	if cfg != null:
		get_node("icon").set_texture(cfg.icon)
	else:
		self.queue_free()

func _on_bonus_body_enter( body ):
	if !body.is_in_group("player"):
		return

	if cfg.has("single") && cfg.single:
		activate_bonus(body)
	else:
		activate_buff(body)

	self.queue_free()

func activate_buff(_body):
	if _body.has_node(bonusName):
		var existsBonus = _body.get_node(bonusName)
		existsBonus.buff_exists(cfg)
		return
	
	var newBuff = cfg.script.new()
	newBuff.set_name(bonusName)
	newBuff.config(cfg)
	_body.add_buff(newBuff)

func activate_bonus(_body):
	var newBonus = cfg.script.new()
	newBonus.config(cfg)
	newBonus.apply(_body)
	newBonus.free()
	
