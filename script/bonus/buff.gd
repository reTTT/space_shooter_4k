extends Node

export var period = 0

var item

func buff_received():
	print("Buff '"+get_name()+"' received "+str(period)) 

func buff_ended():
	print("Buff '"+get_name()+"' ended")	

func buff_exists(_bonus):	
	print("Buff '"+get_name()+"' already received") 
	period = _bonus.period

func config(_bonus):
	period = _bonus.period
	item = _bonus.item.inst.instance()
	item.config(_bonus)

func _process(dt):
	period -= dt
	item.set_time(period)

	if period > 0:
		return

	buff_ended()
	get_parent().remove_buff(self)

func _exit_tree():
	get_node("/root/game/ui/inventory").remove_item(item)

func _ready():
	buff_received()
	if period > 0:
		set_process(true)
	
	get_node("/root/game/ui/inventory").add_item(item)
