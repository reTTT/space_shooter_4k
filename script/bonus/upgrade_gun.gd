extends "res://script/bonus/bonus.gd"

func config (_info):
	pass

func _on_health_body_enter( body ):
	apply(body)

func apply(_body):
	if _body.is_in_group("player"):
		_body.get_node("turret/turret_gun").upgrade()
