extends "res://script/bonus/buff.gd"

var reload = 0.25
var reload_normal = 0.75

func config (_bonus):
	.config(_bonus)
	reload = _bonus.reload

func get_turret():
	return get_node("../turret/turret_gun")

func buff_received():
	print("FireRate received "+str(period))
	var node = get_turret()
	reload_normal = node.reload
	node.reload = reload_normal*0.5
	node.reload_delay = 0.1

func buff_ended():
	print("FireRate ended")
	get_turret().reload = reload_normal
