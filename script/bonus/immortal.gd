extends "res://script/bonus/buff.gd"

func buff_received():
	print("Immortal received "+str(period))
	get_parent().immortal = true

func buff_ended():
	print("Immortal ended")
	get_parent().immortal = false
