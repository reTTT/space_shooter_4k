extends "res://script/bonus/buff.gd"

export(String) var turret_id = "turret_rocket_red"

func buff_received():
	get_node("../turret").enable_turret(turret_id)

func buff_ended():
	get_node("../turret").disable_turret(turret_id)
