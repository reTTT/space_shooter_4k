extends "res://script/bonus/bonus.gd"

export var recover_fully = false

func config (_info):
	health = get_val(_info, "health", 1)	
	recover_fully = get_val(_info, "fully", false)	

func _on_health_body_enter( body ):
	apply(body)

func apply(_body):
	if _body.is_in_group("player"):
		if !recover_fully:
			_body.recover_health(health)
		else:
			var full = _body.get_max_health() - _body.get_health()
			_body.recover_health(full)
