extends Node2D

var wave = -1

export var wave_name = ""
export(NodePath) var layers_path
var wave_data
var layers
var time = 0
var elapsedTime = 0

func get_layers():
	return layers

func get_data():
	if wave_data:
		return wave_data.get_data()
	return null

func get_cur_wave():
	var data = get_data()
	return data[wave]

func get_wave_count():
	var data = get_data()
	return data.size()

func reset():
	wave = -1
	for c in get_children():
		if c.is_in_group("layer"):
			c.reset()

	next_wave()

func get_layer_data(layer_name):
	var cur = get_cur_wave()
	if cur == null:
		return null

	for l in cur.layers:
		if l.name == layer_name:
			return l

	return null

func next_wave():
	elapsedTime = 0.0
	var old = wave
	wave = clamp(wave + 1, 0, get_wave_count()-1)

	if old != wave:
		var cur = get_cur_wave()
		time = cur.time
		emit_signal("change_wave", cur.info)

func _fixed_process(dt):
	elapsedTime += dt
	if time > 0 && elapsedTime >= time:
		next_wave()

func _init():
	add_user_signal("change_wave", ["info"])

func _ready():
	layers = get_node(layers_path)
	wave_data = load(wave_name).new()
	next_wave()
	set_fixed_process(true)
