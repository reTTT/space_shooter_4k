extends Node

func _enter_tree():
	var admob = get_node("/root/admob")
	if admob != null:
		admob.show_banner(true)

func _exit_tree():
	var admob = get_node("/root/admob")
	if admob != null:
		admob.show_banner(false)

