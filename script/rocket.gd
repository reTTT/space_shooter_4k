extends "res://script/layer.gd"

var speed = 600
var dir = Vector2(1, 0)
var damage = 10

var target = null
var state = 0
var timer = 0.2

#func _integrate_forces(s):
#	s.set_linear_velocity(dir*speed)
#	s.set_angular_velocity(0)

func _fixed_process(dt):
	var p = get_pos() + dir * (speed * dt)
	set_pos(p)
	timer -= dt
	if timer <= 0:
		if state == 0:
			timer = 15
			dir = Vector2(0,-1)
			speed = 0
			state = 1
		else:
			self.queue_free()
		return
		
	if state == 0:
		speed -= 4000*dt
		speed = max(speed, 0)
		set_rot(0)
		return

	if target:
		var p1 = target.get_pos()
		var p2 = get_pos()
		var v = Vector2(p1.x-p2.x, p1.y-p2.y)
		v = v.normalized()
		dir = dir.linear_interpolate(v,3.0*dt)
		dir = dir.normalized()
		var d = v.dot(dir)
		if false:#d<0.2:
			speed -= 400*abs(d)*dt
		else:
			speed += 600*d*dt
	else:
		update_target()
		speed += 800*dt
		
	speed = clamp(speed, 200, 400)
	var rot = (atan2(dir.x, dir.y) - deg2rad(180))
	set_rot(rot)

func update_target():
	var targets = get_tree().get_nodes_in_group("enemy")
	if targets.empty():
		return
	target = targets[randi()%targets.size()]
	target.connect("free", self, "free_target")

func free_target(_target):
	target = null

func _ready():
	set_fixed_process(true)

func _on_bullet_body_enter(body):
	if body.is_in_group("ammo") || body.is_in_group("neutral"):
		return
	if body.has_method("add_damage"):
		body.add_damage(damage)
	self.queue_free()

func _on_visibility_notifier_exit_viewport( viewport ):
	self.queue_free()
