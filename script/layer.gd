extends RigidBody2D

export (int, "L_METEOR", "L_BULLET", "L_BONUS", "L_ENEMY", "L_PLAYER") var layer = 0

func get_layer():
	return layer
	
func _ready():
	set_z(get_layer()*10)