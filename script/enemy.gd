extends "res://script/entity.gd"

export var speed = 100
export(int) var score = 10

var explosion_inst = preload("res://prefab/explosion.xscn")

func _ready():
	if  has_node("/root/game"):
		set_target(get_node("/root/game").get_player())

func config(_info):
	if !_info.has("kind"):
		return

	var cfg = get_node("/root/db").get_enemy(_info.kind)
	if cfg == null:
		return

	health = get_val(cfg, "health", 1)
	speed = get_rand_val(cfg, "speed", 100)
	score = get_val(cfg, "score", 10)

	# choose weapon
	var weapon = ""
	
	if _info.has("weapon"):
		weapon = _info.weapon
	elif cfg.has("weapon"):
		weapon = cfg.weapon

	if !weapon.empty():
		cfg = get_node("/root/db").get_weapon(weapon)
		if cfg != null:
			config_weapon(cfg)

	# choose ai
	var ai = ""
	if _info.has("ai"):
		ai = _info.ai
	elif cfg.has("ai"):
		ai = cfg.ai
		
	if !ai.empty():
		cfg = get_node("/root/db").get_ai(ai)
		if cfg != null:
			config_ai(cfg)

func config_weapon(_info):
	var obj = get_node("turret_ai")
	if !obj:
		return

func config_ai(_info):
	pass

func on_kill():
	var expl = explosion_inst.instance()
	expl.set_pos(get_pos())
	expl.set_z(get_z()+1)
	get_parent().add_child(expl)
	get_node("/root/game").add_score(score)
	queue_free()
