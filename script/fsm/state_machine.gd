extends Node

export(NodePath) var path_target
export var start_state = ""
export var start_global_state = ""


var target

var current = null
var previous = null
var global = null

func find_state(state_id):
	for c in get_children():
		if c.get_name() == state_id:
			return c
	
	return null


func change_state(state_id):
	previous = current
	
	if current:
		current.state_exit(target)
	
	current = find_state(state_id)
	
	if current:
		current.state_enter(target)


func revert_to_previous_state():
	if previous:
		change_state(previous.get_name())
	else:
		change_state("")


func current_state():
	return current


func global_state():
	return global


func set_global_state(state_id):
	if global:
		global.state_exit(target)

	global = find_state(state_id)
	
	if global:
		global.state_enter(target)


func previous_state():
	return previous


func get_name_of_current_state():
	return current.get_name()


func _fixed_process(delta):
	if global:
		global.state_execute(target, delta)

	if current:
		current.state_execute(target, delta)


func _ready():
	target = get_node(path_target)
	set_fixed_process(true)
	
	if !start_state.empty():
		change_state(start_state)

	if !start_global_state.empty():
		set_global_state(start_global_state)
