extends Node

var player_inst = preload("res://prefab/player.xscn")

export var spawn_pos = Vector2(240, 700)


func reset():
	var resetable = get_tree().get_nodes_in_group("reset")
	
	for r in resetable:
		if r.has_method("reset"):
			r.reset()


func _create_player():
	var player = player_inst.instance()
	player.set_pos(spawn_pos)
	
	get_node("/root/game/world/objects").add_child(player)


func spawn():
	reset()
	_create_player()


func _ready():
	_create_player()

