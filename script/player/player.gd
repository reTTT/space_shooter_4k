extends "res://script/entity.gd"

var explosion_inst = preload("res://prefab/explosion.xscn")
var screen_rect
var direction = Vector2(0, 0)

func set_direction(dir):
	direction = dir

func get_direction():
	return direction

func on_kill():
	var expl = explosion_inst.instance()
	expl.set_pos(get_pos())
	expl.set_z(get_z())
	get_parent().add_child(expl)
	queue_free()
	get_tree().call_group(SceneTree.GROUP_CALL_REALTIME, "game_over", "on_game_over")

func _integrate_forces(s):
	var lv = s.get_linear_velocity()
	var dir = get_direction()
	lv += dir
	var pos = get_pos()
	if pos.x <= screen_rect.end.x*0.05 && lv.x < 0:
		lv.x = 0
	if pos.x >= screen_rect.end.x*0.95 && lv.x > 0:
		lv.x = 0
	if pos.y <= screen_rect.end.y*0.2 && lv.y < 0:
		lv.y = 0
	if pos.y >= screen_rect.end.y*0.9 && lv.y > 0:
		lv.y = 0

	var friction = lv * -0.05
	lv += friction
	s.set_linear_velocity(lv)

func _ready():
	screen_rect = get_node("/root").get_rect()
	_send_change_health()

func _on_player_body_enter( body ):
	if !body.is_in_group("enemy") && !body.is_in_group("neutral"):
		return
	
	var damage = body.get_health()
	body.kill()
	add_damage(damage)


func _send_change_health():
	get_tree().call_group(SceneTree.GROUP_CALL_REALTIME, "player_health", "change_health", self)

func set_health(health):
	_send_change_health()

func add_damage(damage):
	.add_damage(damage)
	if damage != 0:
		_send_change_health()

func recover_health(health):
	var change = health > 0 && damage
	.recover_health(health)
	if change:
		_send_change_health()

func switch_turret(_script):
	get_node("turret").set_script(_script)
	return get_node("turret")

func _enter_tree():
	get_tree().call_group(SceneTree.GROUP_CALL_REALTIME, "player_listener", "on_player_connect", self)

func _exit_tree():
	get_tree().call_group(SceneTree.GROUP_CALL_REALTIME, "player_listener", "on_player_disconnect", self)
