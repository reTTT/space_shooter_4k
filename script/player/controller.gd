extends Node

export var speed = 30
export var offsetY = -44

var tween
var screen_rect
var ship
var turret

var dest_pos
var joy_offset = Vector2(0, 0)

func clamp_pos(pos):
	var l = screen_rect.end.x*0.05
	var t = screen_rect.end.y*0.2
	var r = screen_rect.end.x*0.95
	var b = screen_rect.end.y*0.9
	
	pos.x = clamp(pos.x, l, r)
	pos.y = clamp(pos.y, t, b)
	
	return pos

func _fixed_process(dt):
	turret.fire()
	
	if joy_offset.length() >= 1:
		var normal = joy_offset.normalized()
		var pos = ship.get_pos() + normal * (dt * speed)
		pos = clamp_pos(pos)
		ship.set_pos(pos)
		dest_pos = clamp_pos(pos + normal * (speed / 2))
		return
	
	var delta = dest_pos - ship.get_pos()
	
	if delta.length() > speed*dt:
		var pos = ship.get_pos() + delta.normalized() * (dt * speed)
		ship.set_pos(pos)
	else:
		ship.set_pos(dest_pos)

func move_to_tween(dest):
	dest = clamp_pos(dest + Vector2(0, offsetY))	
	var delta = dest - ship.get_pos()
	var t = delta.length() / speed
	tween.remove_all()
	tween.interpolate_method(ship, "set_pos", ship.get_pos(), dest, t, "quad", "in_out")
	tween.start()

func move_to(dest):
	dest_pos = clamp_pos(dest + Vector2(0, offsetY))

func _unhandled_input(e):
	if e.type == InputEvent.MOUSE_MOTION && e.button_mask != 0:
		move_to(e.pos)

func _input(e):
	if e.type == InputEvent.JOYSTICK_MOTION:
		if e.axis == 0:
			if abs(e.value) > 0.07:
				joy_offset.x = e.value
			else:
				joy_offset.x = 0
		elif e.axis == 1:
			if abs(e.value) > 0.07:
				joy_offset.y = e.value
			else:
				joy_offset.y = 0

func _ready():
	screen_rect = get_node("/root/global").screen_rect
	ship = get_parent()
	turret = get_node("../turret")
	dest_pos = ship.get_pos()

	if !OS.has_touchscreen_ui_hint():
		set_process_unhandled_input(true)

	set_process_input(true)
	set_fixed_process(true)
