extends Control

export var is_top = false
const BANNER_ID = "ca-app-pub-7768235854069484/6436286859" #"ca-app-pub-7768235854069484/8308489656"

var admob
var sizeOld
var banner = false

func _init():
	if(Globals.has_singleton("AdMob")):
		admob = Globals.get_singleton("AdMob")
		admob.init(true, is_top, BANNER_ID)


func _ready():
	sizeOld = OS.get_video_mode_size()
	set_process(true)


func show_banner(show):
	if banner == show || admob == null:
		return

	banner = show
	admob.showBanner(banner)


func _process(dt):
	var size = OS.get_video_mode_size()
	
	if ((sizeOld.x - size.x) != 0) || ((sizeOld.y - size.y) != 0) :
		sizeOld = size
		if null != admob:
			print(admob.getAdWidth(), ", ", admob.getAdHeight())
			admob.resize(is_top)
