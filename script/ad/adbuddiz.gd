extends Node

const APP_KEY = "" #"b536465a-b197-447c-af10-aa4ed7c39aeb" #"f92c5ea7-8384-4401-a4d0-8d2fe32bfa98"

var adbuddiz = null

func _init():
	if(Globals.has_singleton("AdBuddiz")):
		adbuddiz = Globals.get_singleton("AdBuddiz")
		adbuddiz.init(false)


func show_ad():
	if adbuddiz:
		adbuddiz.showAd()
