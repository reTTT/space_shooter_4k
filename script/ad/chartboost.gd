extends Node

const LOCATION = "LOCATION_DEFAULT"
var chartboost = null

func _init():
	if(Globals.has_singleton("Chartboost")):
		chartboost = Globals.get_singleton("Chartboost")
		chartboost.init()

func showInterstitial():
	if chartboost:
		chartboost.showInterstitial(LOCATION)

func cacheInterstitial():
	if chartboost:
		chartboost.cacheInterstitial(LOCATION)

func isAnyViewVisible():
	if chartboost:
		return chartboost.isAnyViewVisible()
	
	return false

func closeImpression():
	if chartboost:
		chartboost.closeImpression()