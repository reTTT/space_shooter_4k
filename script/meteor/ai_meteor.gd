extends Node

var meteor


func _fixed_process(dt):
	
	var lv = meteor.direction * meteor.speed
	meteor.set_linear_velocity(lv)


func _ready():
	meteor = get_node("../")
	set_fixed_process(true)