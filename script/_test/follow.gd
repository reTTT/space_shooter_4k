extends PathFollow2D

export (int) var select_idx = -1
export var speed = 300
export(bool) var selected = true

func _find_path():
	var path = get_node("/root/game/path")
	var idx = randi() % path.get_child_count()
	
	if select_idx >=0 && select_idx < path.get_child_count():
		idx = select_idx
		
	return path.get_child(idx)

func select_path():
	var p = _find_path()
	get_parent().remove_child(self)
	p.add_child(self)
	set_offset(0)

func _ready():
	if selected:
		set_unit_offset(1)
	set_fixed_process(true)


func _fixed_process(dt):
	var pos = get_offset() + speed * dt
	set_offset(pos)

	if get_unit_offset() >= 1 && selected:
		select_path()
	
	