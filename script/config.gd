extends Node

var config

const FILE_NAME = "user://config.data"
const PASSWORD = "none"

var data = {
	"options": {
		"sound": true,
		"music": true,
	},
}


func get_value(section, key, def):
	var node = data
	
	if section != null && node.has(section):
		node = node[section]
	
	if key != null && node.has(key):
		return node[key]
		
	return def


func set_value(section, key, val):
	if key == null:
		return

	if section != null:
		if !data.has(section):
			data[section] = {}

		data[section][key] = val
	else:
		data[key] = val


func serialize():
	var f = File.new()
	if f.open(FILE_NAME, File.WRITE):
		return

	var json = data.to_json()
	f.store_string(json)
	f.close()


func deserialize():
	var f = File.new()
	if f.open(FILE_NAME, File.READ):
		return

	var json = f.get_line()
	f.close()

	if json.empty():
		return

	data.parse_json(json)


func _ready():
	deserialize()
