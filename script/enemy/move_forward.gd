extends "res://script/fsm/state.gd"

export(Vector2) var dir = Vector2(0, 1)

func state_enter(owner):
	pass

func state_execute(owner, dt):
	var pos = owner.get_pos() + dir * (owner.speed * dt)
	owner.set_pos(pos)
	owner.look_at(pos+dir)

func state_exit(owner):
	pass
