extends "res://script/fsm/state.gd"

var offset = 96
var boost = 0.9
var min_dist = 200

var strafe = -1
var elapsed_time = 0
var duration = 5
var dest = null
var screen_rect

func _target_alive(target):
	if target == null || target.is_dead():
		return false
	return true

func _calc_dest(target, owner):
	var x = target.get_pos().x + offset * strafe
	var y = owner.get_pos().y
	x = clamp(x, screen_rect.pos.x, screen_rect.end.x)
	return Vector2(x, y)

func _calc_dir(target, owner):
	var delta = target.get_pos() - owner.get_pos()
	return sign(delta.x)

func state_enter(owner):
	screen_rect = owner.get_node("/root/global").screen_rect
	var target = owner.get_target()
	if !_target_alive(target):
		get_parent().change_state("move_forward")
		return

	strafe = _calc_dir(target, owner)
	dest = _calc_dest(target, owner)
	min_dist = (target.get_pos() - owner.get_pos()).length() * 0.65
	elapsed_time = 0

func state_execute(owner, dt):
	var target = owner.get_target()
	if !_target_alive(target):
		get_parent().change_state("move_forward")
		return
	
	var target_dist = (target.get_pos() - owner.get_pos()).length()
	
	if target_dist <= min_dist:
		if randf() > 0.75:
			get_parent().change_state("dodge_target")
		else:
			get_parent().change_state("follow_target")
		return
	
	elapsed_time += dt
	
	if elapsed_time > duration:
		get_parent().change_state("move_forward")
		return
	
	var delta = dest - owner.get_pos()
	var len = delta.length()
	var dm = (owner.speed * dt * boost)
	if len < dm:
		owner.set_pos(dest)
		owner.look_at(dest + Vector2(0, 1))
		strafe *= _calc_dir(target, owner)
		dest = _calc_dest(target, owner)
	else:
		var pos = owner.get_pos() + delta.normalized() * dm
		owner.set_pos(pos)
		owner.look_at(pos + Vector2(0, 1))

func state_exit(owner):
	pass