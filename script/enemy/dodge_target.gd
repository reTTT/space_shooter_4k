extends "res://script/fsm/state.gd"

var dist = 64

func state_enter(owner):
	pass

func state_execute(owner, dt):
	var target = owner.get_target()
	if target == null || target.is_dead():
		get_parent().change_state("move_forward")
		return
	
	var delta = target.get_pos() - owner.get_pos()
	
	if abs(delta.x) < dist:
		delta = delta.normalized() * -1
		delta.y = 1
	else:
		delta = Vector2(0, 1)

	var pos = owner.get_pos() + delta * (owner.speed * dt)
	owner.set_pos(pos)

func state_exit(owner):
	pass