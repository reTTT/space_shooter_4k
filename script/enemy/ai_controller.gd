extends Node

var ship

func _enter_tree():
	ship = get_parent()
	
	
func config(_name):
	pass


func start_ai(ai_id):
	if ai_id == null && ai_id.empty():
		return

	for c in get_children():
		if c.get_name() == ai_id:
			c.activate(true)


func on_activate(ai):
	pass


func on_deactivate(ai):
	pass


func get_target(def = ship):
	var players = get_tree().get_nodes_in_group("player")
	
	if players.empty():
		return def
	
	var target = players[randi()%players.size()]
	
	if !target.is_type("RigidBody2D"):
		return def
	
	if target.is_dead():
		return def
	
	return target
	