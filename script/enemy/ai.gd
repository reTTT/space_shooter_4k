extends Node

var fsm

var table = [
	"attack",
	"follow_target",
	"move_forward",
]

var min_dist = 0.0

func _ready():
	fsm = get_node("../fsm")
	set_fixed_process(true)
	min_dist = rand_range(250, 350)

func _fixed_process(dt):
	var target = get_parent().get_target()
	if target == null || target.is_dead(): 
		return
		
	if fsm.current_state().get_name() != "move_forward":
		return
	
	var dist = target.get_pos().distance_to(get_parent().get_pos())
	if dist >= (min_dist) && dist <= (min_dist + 150):
		var idx = randi() % table.size()
		fsm.change_state(table[idx])
		set_fixed_process(false)
