extends "res://script/enemy/ai_controller.gd"

var fn_steps = [
	"steps_0",
	"steps_1",
	"steps_2",
	"steps_3",
	"steps_4",
]

var step = 0
var lr_count = 0

func run_step():
	if step >= fn_steps.size():
		return
		
	call(fn_steps[step])

func _ready():
	run_step()


func on_deactivate(ai):
	step += 1
	run_step()


func steps_0():
	
	if randf() < 0.35:
		get_node("move_by").destination = Vector2(0, 1024)
		start_ai("move_by")
		step = 1
	else:
		get_node("move_by").destination = Vector2(0, rand_range(300, 450))
		start_ai("move_by")
		lr_count = rand_range(3, 6)


func steps_1():
	var target = get_target()
	
	var dir = sign(target.get_pos().x - ship.get_pos().x)
	var x = 0
	
	if dir < 0:
		x = rand_range(0, ship.get_pos().x) * dir
	elif dir > 0:
		x = (rand_range(ship.get_pos().x, 480) - ship.get_pos().x) * dir
	
	get_node("move_by").destination = Vector2(x, 0)
	start_ai("move_by")
	
	lr_count -= 1
	
	if lr_count > 0:
		step = 1


func steps_2():
	get_node("move_by").destination = Vector2(0, 854)
	start_ai("move_by")


func steps_3():
	ship.queue_free()
