extends "res://script/enemy/ai_base.gd"

export var time = 1

var target
var elapsed_time = 0

func on_activate():
	target = get_parent().get_target(null)
	
	if target == null:
		activate(false)
		get_parent().start_ai(goto)
		return

	elapsed_time = 0
	set_fixed_process(true)
	.on_activate()


func on_deactivate():
	set_fixed_process(false)
	.on_deactivate()
	

func _fixed_process(dt):
	elapsed_time += dt
	
	if elapsed_time >= time || target.is_dead():
		activate(false)
		get_parent().start_ai(goto)
		return

	var delta = target.get_pos() - ship.get_pos()
	delta.y = 0
	
	var len = delta.length()
	var move = dt*ship.speed
	
	if len > move:
		var pos = ship.get_pos() + delta.normalized() * move
		ship.set_pos(pos)
	else:
		var pos = target.get_pos()
		pos.y = ship.get_pos().y
		ship.set_pos(pos)
		
	