extends "res://script/enemy/ai_controller.gd"

var fn_steps = [
	"steps_0",
	"steps_1",
	"steps_2",
	"steps_3",
]

var step = 0

func run_step():
	if step >= fn_steps.size():
		return
		
	call(fn_steps[step])

func _ready():
	run_step()


func on_deactivate(ai):
	step += 1
	run_step()


func steps_0():
	if randf() < 0.5:
		get_node("move_by").destination = Vector2(0, 1024)
		start_ai("move_by")
		step = 2
	else:
		get_node("move_by").destination = Vector2(0, rand_range(300, 450))
		start_ai("move_by")


func steps_1():
	get_node("follow_target").time = rand_range(1, 2)
	start_ai("follow_target")


func steps_2():
	get_node("move_by").destination = Vector2(0, 854)
	start_ai("move_by")


func steps_3():
	ship.queue_free() 