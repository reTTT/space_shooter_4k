extends "res://script/enemy/ai_base.gd"

export(Vector2) var destination = Vector2(0, 0)

var finish = Vector2(0, 0)


func on_activate():
	finish = ship.get_pos() + destination
	set_fixed_process(true)
	.on_activate()


func on_deactivate():
	set_fixed_process(false)
	.on_deactivate()


func _fixed_process(dt):
	var delta = finish - ship.get_pos()
	var len = delta.length()
	var move = dt*ship.speed
	
	if len > move:
		var pos = ship.get_pos() + delta.normalized() * move
		ship.set_pos(pos)
	else:
		ship.set_pos(finish)
		activate(false)
		get_parent().start_ai(goto)
		