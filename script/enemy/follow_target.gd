extends "res://script/fsm/state.gd"

var max_angle = 45
var boost = 1.25

func state_enter(owner):
	pass

func state_execute(owner, dt):
	var target = owner.get_target()

	if target == null || target.is_dead():
		get_parent().change_state("move_forward")
		return
	
	var tp = target.get_pos()
	var op = owner.get_pos()

	if tp.y <= op.y: #|| abs(tp.x - op.x) < 4:
		get_parent().change_state("move_forward")
		return

	var delta = tp - op
	delta = delta.normalized()
	var angle = rad2deg(delta.angle())
	angle = clamp(abs(angle), 0, max_angle) * sign(angle)
	delta = Vector2(0, 1).rotated(deg2rad(angle))
	var pos = owner.get_pos() + delta * (owner.speed * boost * dt)
	owner.set_pos(pos)
	owner.look_at(pos + Vector2(0, 1))

func state_exit(owner):
	pass
