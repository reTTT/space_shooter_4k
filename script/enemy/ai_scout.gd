extends "res://script/enemy/ai_controller.gd"

var fn_steps = [
	"steps_0",
	"steps_1",
	"steps_2",
]

var step = 0
var move = null
var chance_passive = 0.3


func config(_info):
	chance_passive = _info.chance	


func run_step():
	if step >= fn_steps.size():
		return
	
	call(fn_steps[step])


func _ready():
	move = get_node("move_by")
	
	run_step()	


func on_deactivate(ai):
	step += 1
	run_step()


func steps_0():
	if randf() < chance_passive:
		move.destination = Vector2(0, 1024)
		step = 1
	else:		
		move.destination = Vector2(0, rand_range(200, 350))
		
	move.activate(true)


func steps_1():
	var target = get_target()
	
	var dir = sign(target.get_pos().x - ship.get_pos().x)
	var x = 0
	
	# повышена активность при смещении 
	if dir < 0:
		x = rand_range(ship.get_pos().x/2.0, ship.get_pos().x) * dir
	elif dir > 0:
		x = (rand_range((ship.get_pos().x+480)/2.0, 480) - ship.get_pos().x) * dir
		
	move.destination = Vector2(x, 854)
	move.activate(true)


func steps_2():
	ship.queue_free()
