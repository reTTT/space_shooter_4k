extends Node

export var goto = ""

var ship
var active = false

export var auto_run = false

func is_active():
	return active

func activate(active):
	if active == is_active():
		return
	
	self.active = active

	if active:
		on_activate()
	else:
		on_deactivate()
	

func _ready():
	ship = get_parent().ship
	
	if auto_run:
		activate(true)


func on_activate():
	get_parent().on_activate(self)


func on_deactivate():
	get_parent().on_deactivate(self)
