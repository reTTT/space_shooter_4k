extends "res://script/fsm/state.gd"

var rot = 90

func state_enter(owner):
	pass

func state_execute(owner, dt):
	var pos = owner.mule.get_global_pos()
	owner.set_global_pos(pos)
	owner.set_rot(owner.mule.get_rot()-rot)

func state_exit(owner):
	pass
