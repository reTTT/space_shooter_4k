extends Node

const S_PAUSE = 0 
const S_PLAY = 1

const DELAY = 2

var state = S_PLAY

var score = 0
var best_score = 0
var need_serialize = true
var serialize_delay = DELAY

func set_state(state):
	if self.state != state:
		self.state = state
		_apply_state()

func get_state():
	return state

func _apply_state():
	if state == S_PLAY:
		get_tree().set_pause(false)
	elif state == S_PAUSE:
		get_tree().set_pause(true)

func get_player():
	return get_node("/root/game/world/objects/player")

func _init():
	add_user_signal("change_score", ["score"])
	add_user_signal("change_best_score", ["score"])

func _fixed_process(dt):
	serialize_delay -= dt
	if need_serialize && serialize_delay <= 0:
		need_serialize = false
		serialize_delay = DELAY
		get_node("/root/config").serialize()

func add_score(score):
	self.score += score
	emit_signal("change_score", self.score)
	set_best_score(self.score)

func apply_best_score():
	get_node("/root/config").set_value("game", "best_score", best_score)
	emit_signal("change_best_score", best_score)

func set_best_score(score):
	if score < best_score:
		return

	best_score = score
	need_serialize = true
	apply_best_score()
	
	if has_node("/root/gps"):
		get_node("/root/gps").lb_submit(best_score)

func _serialize():
	if !need_serialize:
		return
	need_serialize = false
	get_node("/root/config").serialize()

func reset():
	set_best_score(score)
	apply_best_score()
	_serialize()
	score = 0
	emit_signal("change_score", score)

func _ready():
	best_score = get_node("/root/config").get_value("game", "best_score", 0)
	reset()

func _exit_tree():
	get_node("/root/config").serialize()

func _on_laser_pressed():
	var player = get_node("/root/game/world/player_spawn/player")
	if !player:
		return

	var turret = player.get_node("turret")
	turret.activate_turret("turret_laser")

func _on_rocket_pressed():
	var player = get_node("/root/game/world/player_spawn/player")
	if !player:
		return

	var turret = player.get_node("turret")
	turret.activate_turret("turret_rocket")
