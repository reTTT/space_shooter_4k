extends Node2D

func _on_change_wave(info):
	get_node("text").set_text(info.name)
	get_node("ani").play("show")


func _ready():
	if has_node("/root/game/world/enemy_spawn"):
		get_node("/root/game/world/enemy_spawn").connect("change_wave", self, "_on_change_wave")
