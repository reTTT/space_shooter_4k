extends PathFollow2D

var ship

func _on_dead():
	set_fixed_process(false)
	queue_free()

func _ready():
	ship.connect("dead", self, "_on_dead")
	set_fixed_process(true)

func _fixed_process(dt):
	var offset = get_offset() + ship.speed * dt
	set_offset(offset)
	var old = ship.get_global_pos()
	var pos = get_global_pos()
	ship.look_at(pos)
	ship.set_global_pos(pos)

	if get_unit_offset() >= 1:
		ship.queue_free()
		_on_dead()

func reset():
	_on_dead()