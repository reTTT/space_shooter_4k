extends "res://script/formation/formation.gd"


var start = 0
var step = 25
var radius = 128

var idx = 0


func config(info):
	.config(info)
	start = get_val(info.spawn, "start", 0)
	step = get_val(info.spawn, "step", 25)
	radius = get_val(info.spawn, "radius", 96)

	create_group(group)
	queue_free()


func create_pos(idx):
	var angle = deg2rad(idx * step + start)
	var pos = Vector2(cos(angle), sin(angle)) * radius
	return loc_to_global(pos)


func init_spawn_object(obj, info):
	var pos = create_pos(idx)
	obj.set_pos(pos)
	obj.config(info)
	get_parent().add_child(obj)
	idx += 1
