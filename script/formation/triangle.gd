extends "res://script/formation/formation.gd"

var idx = 1

var offset = Vector2(96, 96)

func config(info):
	.config(info)

	if info.spawn.has("offset"):
		offset = Vector2(info.spawn.offset[0], info.spawn.offset[1])

	create_group(group)
	queue_free()


func create_pos(idx):
	var pos = offset * (idx/2)

	if idx % 2:
		pos.x *= -1
	
	pos.y *= -1

	return loc_to_global(pos)


func init_spawn_object(obj, info):
	var pos = create_pos(idx)
	obj.set_pos(pos)
	obj.config(info)
	get_parent().add_child(obj)
	idx += 1

