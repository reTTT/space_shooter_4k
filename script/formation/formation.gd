extends "res://script/spawn/base_spawn.gd"

var group


func config(info):
	self.group = info


func get_val(data, var_id, def):
	if !data.has(var_id):
		return def

	return data[var_id]


func loc_to_global(loc):
	var t = get_global_transform()
	var glob = t.xform(loc)
	t = get_parent().get_global_transform().inverse()
	glob = t.xform(glob)
	return glob
