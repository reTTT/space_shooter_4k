extends "res://script/formation/formation.gd"

var col = 2
var row = 2
var offset = Vector2(96, 96)

var idx = 0

func config(info):
	.config(info)
	col = get_val(info.spawn, "col", 0)
	row = get_val(info.spawn, "row", 25)

	if info.spawn.has("offset"):
		offset = Vector2(info.spawn.offset[0], info.spawn.offset[1])

	create_group(group)
	queue_free()


func create_pos(idx):
	var start = Vector2(offset.x * (col/2*-1), 0)
	var x = idx % col
	var y = idx / col * -1
	var pos = Vector2(offset.x*x, offset.y*y) + start
	return loc_to_global(pos)


func init_spawn_object(obj, info):
	var pos = create_pos(idx)
	obj.set_pos(pos)
	obj.config(info)
	get_parent().add_child(obj)
	idx += 1