extends Label

func _on_change_best_score(score):
	set_text(TS.translate("ID_SCORE") + ": " + str(score))


func _enter_tree():
	get_node("/root/game").connect("change_best_score", self, "_on_change_best_score")
