extends Label

func change_health(player):
	var txt = TS.translate("ID_HEALTH") + ": " + str(player.get_health())
	set_text(txt)
