extends TextureProgress

func change_health(player):
	set_val(player.get_health_in_percent())


func _ready():
	set_max(1)
	set_step(0.1)
