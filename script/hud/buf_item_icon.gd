extends Control


func config(info):
	if info == null:
		return
	
	get_node("icon").set_texture(info.icon)


func set_time(time):
	var minutes = int(time) / 60
	var secunds = int(time) % 60
	
	#var t = str(minutes,":", secunds)
	var t = str(ceil(time))
	get_node("txt").set_text(t)

