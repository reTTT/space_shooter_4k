extends Node

var ship
var turret

export var speed_h = 20
export var speed_v = 10

var mouse_delta = null
var mause_sensitivity = 1

var joystick
var btn_fire

func user_move():
	var dir = Vector2(0, 0)

	if mouse_delta != null:
		dir.x = sign(mouse_delta.x)*speed_h*mause_sensitivity
		dir.y = sign(mouse_delta.y)*speed_v*mause_sensitivity
		mouse_delta = null
		ship.set_direction(dir)
		return

	if Input.is_action_pressed("move_left"):
		dir.x -= speed_h
	elif Input.is_action_pressed("move_right"):
		dir.x += speed_h

	if Input.is_action_pressed("move_up"):
		dir.y -= speed_v
	elif Input.is_action_pressed("move_down"):
		dir.y += speed_v

	ship.set_direction(dir)


func user_fire():
	if turret.can_fire():
		turret.fire()


func _process(dt):
	user_move()
	user_fire()


func _input(e):
	if e.type == InputEvent.MOUSE_MOTION:
		mouse_delta = e.relative_pos


func _ready():
	ship = get_parent()
	turret = ship.get_node("turret")
	set_process(true)
	set_process_input(true)
	
	var joystick_groups = get_tree().get_nodes_in_group("joystick")
	
	for j in joystick_groups:
		joystick = j
		break

	var btn_fire_groups = get_tree().get_nodes_in_group("btn_fire")
	
	for b in btn_fire_groups:
		btn_fire = b
		break


