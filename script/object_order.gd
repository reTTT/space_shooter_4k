extends Node

var need_sort = false

func sort(a, b):
	if !a.has_method("get_layer") || !b.has_method("get_layer"):
		return false

	if a.layer < b.layer:
		return true

	return false


func _fixed_process(delta):
	if need_sort:
		need_sort = false
		var children = get_children()
		children.sort_custom(self, "sort")
		
		var pos = 0
		for child in children:
			move_child(child, pos)
			pos += 1


func add_child(child):
	.add_child(child)
	need_sort = true


func _ready():
	set_fixed_process(true)
	