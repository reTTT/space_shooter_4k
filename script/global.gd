extends Node

var screen_rect = Rect2(0, 0, 0, 0)

func _ready():
	var cfg = get_node("/root/config")
	
	var sound = cfg.get_value("options", "sound", true)
	var music = cfg.get_value("options", "music", true)
	AS.set_stream_global_volume_scale(music)
	AS.set_fx_global_volume_scale(sound)
	
	var rect = get_tree().get_root().get_rect()
	var vrect = get_tree().get_root().get_visible_rect()
	var aspect = vrect.size.width / rect.size.width
	screen_rect.size.width = aspect * rect.size.width
	screen_rect.size.height = aspect * rect.size.height 
