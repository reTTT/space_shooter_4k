extends "res://script/layer.gd"

export var damage = 0
var dead = false
var last_damage = 0
var immortal_period = 0

export var health = 1
export var immortal = false

var target

func _target_dead():
	set_target(null)

func set_target(target):
	if self.target:
		self.target.disconnect("dead", self, "_target_dead")
	
	self.target = target
	
	if self.target:
		self.target.connect("dead", self, "_target_dead")

func get_target():
	return target

func add_buff(buff):
	if buff == null:
		return

	add_child(buff)
	emit_signal("add_buff", buff)

func remove_buff(buff):
	if buff == null:
		return

	emit_signal("remove_buff", buff)
	buff.queue_free()

func _init():
	add_user_signal("damage")
	add_user_signal("dead")
	add_user_signal("free")
	add_user_signal("change_health")
	add_user_signal("add_buff", ["buff"])
	add_user_signal("remove_buff", ["buff"])

func is_dead():
	return dead

func get_max_health():
	return health

func get_health():
	return health - damage

func get_health_in_percent():
	if health <= 0:
		return 0
	return 1.0 / health * get_health()

func set_health(health):
	if health != self.health:
		self.health = health
		emit_signal("change_health")

func is_imortal():
	var tick = OS.get_ticks_msec()
	return tick-last_damage<immortal_period

func add_damage(damage):
	if immortal:
		return

	if is_imortal():
		return

	self.damage = clamp(self.damage+damage, 0, health)
	last_damage = OS.get_ticks_msec()
	
	if !dead && !get_health():
		kill()
	else:
		emit_signal("change_health")
		emit_signal("damage")

func recover_health(health):
	if damage > 0 && health > 0:
		damage = clamp(damage-health, 0, self.health)
		emit_signal("change_health")

func kill():
	if !dead:
		dead = true
		emit_signal("dead")
		on_kill()

func _exit_tree():
	emit_signal("free", self)

# Additional configuration object after generation
func config(info):
	pass

func get_val(data, var_id, def):
	if !data.has(var_id):
		print ("Value '"+var_id+"' not found. Def: "+str(def))
		return def

	return data[var_id]

func get_rand_val(data, var_id, def):
	if !data.has(var_id):
		print ("Value '"+var_id+"' not found. Def: "+str(def))
		return def

	return rand_range(data[var_id][0],data[var_id][1])

func on_kill():
	pass
