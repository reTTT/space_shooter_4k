extends Sprite

var disabled = false
var player
var touch_idx = -1
export var offset_h = 32

func is_inside(pos):
	var t = self.get_global_transform().inverse()
	var p = t.xform(pos)
	var r = self.get_item_rect()
	
	return r.has_point(p) 

func on_player_connect(player):
	if disabled:
		return
	
	self.player = player
	on_show()
	touch_idx = -1


func on_player_disconnect(player):
	if disabled:
		return
	
	if self.player == player:
		self.player = null
		set_opacity(0)
	
	touch_idx = -1


func on_show():
	if player == null:
		return

	var pos
	
	if player.get_node("controller").dest_pos != null:
		pos = player.get_node("controller").dest_pos
	else:
		pos = player.get_pos()
	
	set_pos(pos + Vector2(0, player.get_item_rect().size.height + offset_h))
	get_node("ani").play("show")


func _input(e):
	if disabled:
		return

	if e.type == InputEvent.SCREEN_TOUCH:
		if e.is_pressed():
			if touch_idx == -1 && is_inside(e.pos):
				touch_idx = e.index
				get_node("ani").play("hide")
		else:
			if touch_idx == e.index:
				touch_idx = -1
				on_show()
	elif e.type == InputEvent.SCREEN_DRAG:
		if e.index == touch_idx && player != null:
			player.get_node("controller").move_to(e.pos)



func _ready():
	disabled = !OS.has_touchscreen_ui_hint()
	set_opacity(0)
	set_process_input(true)
	