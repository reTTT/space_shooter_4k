extends Node

const LEADERBOARD_ID	= "CgkIz42V6OYcEAIQAQ"	# Place your Leaderboard ID here

var gps

func _ready():
	if(Globals.has_singleton("GooglePlayService")):
		gps = Globals.get_singleton("GooglePlayService")
		gps.init()
	else:
		gps = null


func sign_in():
	if null == gps:
		return
	gps.signin()


func sign_out():
	if null == gps:
		return
	gps.signout()


func lb_submit(score):
	if null == gps || score <= 0:
		return
	
	gps.lbSubmit(LEADERBOARD_ID, int(score))


func lb_show():
	if null == gps:
		return
	
	gps.lbShow(LEADERBOARD_ID)

