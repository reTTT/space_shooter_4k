extends "res://script/enemy.gd"

export(Vector2) var direction = Vector2(0, 1)

var dir_angular = 0
var speed_angular = 0

func _fixed_process(dt):
	if dir_angular != 0:
		set_angular_velocity(dir_angular*speed_angular)

func config(info):
	if !info.has("kind"):
		return

	var cfg = get_node("/root/db").get_meteor(info.kind)
	if cfg == null:
		return

	var img = load(cfg.tiles[randi()%cfg.tiles.size()])
	var body = get_node("body");
	body.set_texture(img);
	body.set_flip_h(randi()%2);

	var s = get_rand_val(cfg, "scale", 1.0)
	body.set_scale(Vector2(s,s))
	#set_scale(Vector2(s,s))

	score = cfg.score
	health = cfg.health
	speed = get_rand_val(cfg, "speed", 100.0) # rand_range(cfg.speed[0],cfg.speed[1])

	clear_shapes()
	var shape = CircleShape2D.new()
	shape.set_radius(cfg.radius*s)
	add_shape(shape)

	if !(cfg.has("drop")):
		var drop = get_node("drop")
		remove_child(drop)
		drop.free()


func _ready():
	var angle = deg2rad(rand_range(0, 360))
	set_rot(angle)

	if randf() < 0.5:
		dir_angular = randf() * -1
	else:
		dir_angular = randf()
	
	speed_angular = rand_range(3, 7) / (1.0 / get_node("body").get_scale().x)
	
	if randf() > 0.85:
		dir_angular = 0

	set_fixed_process(true)
