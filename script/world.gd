extends Node

func reset():

	var objects = get_node("objects")
	
	for obj in objects.get_children():
		obj.queue_free()


func _on_player_body_enter( body ):
	reset()

