extends Sprite

var ani_fade
var stars_in = false

func _ready():
	ani_fade = get_node("ani_fade")
	set_opacity(1)

func _on_fade_timer_timeout():
	if stars_in:
		ani_fade.play("fade_out")
		stars_in = false
	else:
		ani_fade.play("fade_in")
		stars_in = true
