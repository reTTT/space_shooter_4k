extends "res://script/layer.gd"

var speed = 600
var dir = Vector2(0, -1)
var damage = 10

func _integrate_forces(s):
	pass

func _apply_rot():
	var rot = (atan2(dir.x, dir.y) - deg2rad(180))
	set_rot(rot)

func _fixed_process(dt):
	var p = get_pos() + dir * (speed * dt)
	set_pos(p)
	_apply_rot()

func _ready():
	_apply_rot()
	set_fixed_process(true)

func _on_bullet_body_enter(body):
	if body.has_method("add_damage"):
		body.add_damage(damage)

	self.queue_free()

func _on_VisibilityNotifier2D_exit_viewport( viewport ):
	self.queue_free()
