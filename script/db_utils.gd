extends Node

var db_Meteor = load("res://db/db_meteor.gd").new().get_data()
var db_Enemy = load("res://db/db_enemy.gd").new().get_data()
var db_Weapon = load("res://db/db_weapon.gd").new().get_data()
var db_Bonus = load("res://db/db_bonus.gd").new().get_data()
var db_AI = load("res://db/db_ai.gd").new().get_data()

func get_meteor (_kind):
	var cur = null
	for kind in db_Meteor:
		if kind.name == _kind:
			cur = kind
			break
	if cur == null:
		print ("Error get_meteor - kind not found: "+_kind)
		return null
	return cur
	
func get_enemy (_kind):
	var cur = null
	for kind in db_Enemy:
		if kind.name == _kind:
			cur = kind
			break
	if cur == null:
		print ("Error get_enemy - kind not found: "+_kind)
		return null
	return cur

func get_weapon (_kind):
	var cur = null
	for kind in db_Weapon:
		if kind.name == _kind:
			cur = kind
			break
	if cur == null:
		print ("Error get_weapon - kind not found: "+_kind)
		return null
	return cur

func get_bonus (_kind):
	var cur = null
	for kind in db_Bonus:
		if kind.name == _kind:
			cur = kind
			break
	if cur == null:
		print ("Error get_bonus - kind not found: "+_kind)
		return null
	return cur

func get_ai(_kind):
	var cur = null
	for kind in db_AI:
		if kind.name == _kind:
			cur = kind
			break
	if cur == null:
		print ("Error get_ai - kind not found: "+_kind)
		return null
	return cur

func _enter_tree():
	print("create db")


func _exit_tree():
	print("destroy db")