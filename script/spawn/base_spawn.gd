extends Node

var data
var group_created = {}

func reset():
	group_created = {}


func select_group(groups):
	var full_chance = 0
	
	var active_groups = []
	
	for group in groups:
		if group.has("count"):
			var id = group["id"]
			if group_created.has(id) && group_created[id] >= group.count:
				continue

		active_groups.append(group)
		full_chance += group.chance
	
	var percent = randf() * full_chance
	var chance = 0
	
	for group in active_groups:
		chance += group.chance
		
		if chance > percent:
			return group
			
	return null


func create_group(group):
	if group == null:
		return

	for info in group.entity:
		create_object(info)

	if group.has("id"):
		var id = group["id"]
		if !group_created.has( id ):
			group_created[id] = 0

		group_created[id] += 1


func create_object(info):
	if info.has("count"):
		for i in range(info.count):
			var obj = info.inst.instance()
			init_spawn_object(obj, info)
	else:
		var obj = info.inst.instance()
		init_spawn_object(obj, info)


func init_spawn_object(obj, info):
	pass
