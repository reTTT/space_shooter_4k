extends "res://script/spawn/base_spawn.gd"

export(Script) var db_file
var data_base = null


func drop():
	var group = select_group(data_base.get_data())
	create_group(group)


func _ready():
	if db_file!= null:
		data_base = db_file.new()
	
	get_parent().connect("dead", self, "drop") 
	

func init_spawn_object(obj, info):
	var owner = get_parent()
	obj.set_pos(owner.get_pos())
	
	owner.get_parent().add_child(obj)
	obj.config(info)
	var angle = deg2rad(rand_range(0, 360))
	obj.get_node("fsm/move_forward").dir = Vector2(sin(angle), cos(angle))
	obj.speed *= rand_range(1.5, 2)
	
