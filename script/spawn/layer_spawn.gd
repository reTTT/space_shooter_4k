extends "res://script/spawn/base_spawn.gd"

var delay

var mule = preload("res://prefab/mule.xscn")
var screen_rect = Rect2(0, 0, 480, 854)

func reset_delay():
	if data == null:
		return

	var d = data["delay"]

	if d.size() == 1:
		delay = d[0]
	elif d.size() > 1:
		delay = rand_range(d[0], d[1])
	else:
		delay = 1

func reset():
	.reset()
	data = null

func _fixed_process(dt):
	if data == null:
		return
	delay -= dt
	if delay <= 0:
		spawn()

func create_formation(group):
	var obj = group.spawn.inst.instance()
	init_spawn_object(obj, group)

func spawn():
	reset_delay()
	var group = select_group(data.groups)
	if group == null:
		return
	if group.has("spawn"):
		create_formation(group)
	else:
		create_group(group)

func _select_path(idx):
	var path = get_node("/root/game/world/path")	
	if idx == -1:
		idx = randi() % path.get_child_count()
	return path.get_child(idx)

func init_spawn_object(obj, info):
	var path = null	
	if info.has("path"):
		var idx = info["path"]
		path = _select_path(idx)
	if path:
		var m = mule.instance()
		m.ship = obj
		get_parent().get_layers().add_child(obj)
		obj.remove_child(obj.get_node("fsm"))
		if obj.has_node("ai"):
			obj.get_node("ai").queue_free()
		var pos = Vector2(screen_rect.size.width * -0.1, screen_rect.size.height * -0.1)
		obj.set_pos(pos)
		path.add_child(m) 
	else:
		var l = screen_rect.size.width * 0.05
		var r = screen_rect.size.width * 0.95
		var pos = Vector2(rand_range(l, r), screen_rect.size.height * -0.1)
		obj.set_pos(pos)
		get_parent().get_layers().add_child(obj)
		obj.config(info)

func _on_change_wave(info):
	data = get_parent().get_layer_data(get_name())
	if data != null:
		for g in data.groups:
			if !g.has("id"):
				g["id"] = g.hash()

	reset_delay()

func _ready():
	screen_rect = get_node("/root/global").screen_rect
	get_parent().connect("change_wave", self, "_on_change_wave")
	reset()
	set_fixed_process(true)
