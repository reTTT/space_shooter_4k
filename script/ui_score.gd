extends Label

export var text = "ID_SCORE"
export var slot = "change_score"

func _on_change_score(score):
	set_text(TS.translate(text) + ": " + str(score))


func _enter_tree():
	_on_change_score(0)
	get_node("/root/game").connect(slot, self, "_on_change_score")
