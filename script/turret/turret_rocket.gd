extends "res://script/turret/turret.gd"

export var trunk = []
var trunk_node = []

func _create_rocket(pos, dir):
	var bullet = bullet_inst.instance()
	bullet.add_collision_exception_with( get_ship() )
	bullet.set_global_pos(pos)
	bullet.dir = dir
	bullet.set_collision_mask(collision_mask)
	bullet.damage = damage
	
	bullets.add_child(bullet)


func fire():
	var pos = get_global_pos()
	_create_rocket(pos, dire)
	_create_rocket(pos, dir)

	reload_delay = reload
	_sfx_play()
