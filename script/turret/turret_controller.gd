extends Node2D

export var def_active = "turret_laser"

func fire():
	for t in get_children():
		if t.can_fire():
			t.fire()


func can_fire():
	for t in get_children():
		if t.can_fire():
			return true
	
	return false


func switch_turret(name):
	for t in get_children():
		if t.get_name() == name:
			t.active = true
		else:
			t.active = false


func enable_turret(name):
	for t in get_children():
		if t.get_name() == name:
			t.active = true


func disable_turret(name):
	for t in get_children():
		if t.get_name() == name:
			t.active = false

func _ready():
	#switch_turret(def_active)
	pass