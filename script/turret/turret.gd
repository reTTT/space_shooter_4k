extends Node2D

var delay = 0
var volume = 0.1

var bullets
var reload_delay = 0
var sfx

export(PackedScene) var bullet_inst = preload("res://prefab/bullet/bullet_blue.xscn")
export(int) var collision_mask = 0
export var direction = Vector2(0, -1)
export(bool) var user = true
export(float) var damage = 10
export var reload = 0.5
export(bool) var active = true
export var sfx_id = "sfx_laser1"

func set_reload_deley(delay):
	reload_delay = delay

func can_fire():
	return reload_delay <= 0 && active

func get_ship():
	return get_parent().get_parent();

func fire():
	reload_delay = reload

func _sfx_play():
	if sfx:
		var id = sfx.play(sfx_id)
		sfx.voice_set_volume_scale_db(id, volume)

func _fixed_process(dt):
	reload_delay -= dt

func _ready():
	reload_delay = rand_range(0, reload)
	bullets = get_node("/root/game/world/objects")
	if has_node("sfx"):
		sfx = get_node("sfx")
	set_fixed_process(true)
