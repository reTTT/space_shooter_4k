extends "res://script/turret/turret.gd"

var spr_ray
var spr_end

var life = 0

func update_laser(dt):
	var s = get_world_2d().get_direct_space_state()
	var from = get_global_pos()
	var to = from + Vector2(0, -1) * 1280
	var target = s.intersect_ray(from, to, [], 6)

	if !target.empty():
		to = target.position
		target.collider.add_damage(damage)

	var center = (from + to) * 0.5
	var pos = get_global_transform().xform_inv(center)
	var rot = center.angle_to_point(center)
	var height = float(spr_ray.get_texture().get_height())
	var ray_scale = Vector2(0.25, (to-from).length() / height)
	
	spr_ray.set_rot(rot)
	spr_ray.set_scale(ray_scale)
	spr_ray.set_pos(pos)
	
	var end_pos = get_global_transform().xform_inv(to)
	
	spr_end.set_pos(end_pos)


func fire():
	_show_laser()
	.fire()
	life = 0.15
	reload_delay += life


func _fixed_process(dt):
	if life > 0:
		life -= dt
		update_laser(dt)
		
		if life < 0:
			_hide_laser()


func _hide_laser():
	spr_ray.hide()
	spr_end.hide()


func _show_laser():
	spr_ray.show()
	spr_end.show()
	spr_end.set_rot(randf()*PI*2)


func _ready():
	spr_ray = get_node("ray")
	spr_end = get_node("end")
	_hide_laser()
	