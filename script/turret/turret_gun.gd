extends "res://script/turret/turret.gd"

var upgrades = []
var cur_upgrade_id = 0

func upgrade():
	if cur_upgrade_id + 1 < upgrades.size():
		cur_upgrade_id += 1

func _create_bullet(pos, dir):
	var bullet = bullet_inst.instance()
	bullet.set_global_pos(pos)
	bullet.add_collision_exception_with( get_ship() )
	bullet.dir = dir
	bullet.set_collision_mask(collision_mask)
	bullet.damage = damage
	bullets.add_child(bullet)

func _fire(node):
	var pos = node.get_global_pos()
	var t = node.get_global_transform()
	var dir = direction.rotated(t.get_rotation())
	_create_bullet(pos, dir)

func fire():
	if upgrades.empty():
		return
	var trunk = upgrades[cur_upgrade_id]
	if trunk.empty():
		_fire(self)
	else:
		for n in trunk:
			_fire(n)

	.fire()
	_sfx_play()

func _ready():
	for u in get_children():
		if u.is_in_group("upgrade"):
			var trunk = []
			for t in u.get_children():
				if t.is_in_group("trunk"):
					trunk.append(t)
			if !trunk.empty():
				upgrades.append(trunk)
