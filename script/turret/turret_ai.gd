extends Node

export var fire_dalay = 10

var turret = []


func _fixed_process(dt):
	for t in turret:
		if t.can_fire():
			t.fire()


func _ready():
	for child in get_children():
		if child.is_in_group("turret"):
			turret.append(child)
 
	set_fixed_process(true)
