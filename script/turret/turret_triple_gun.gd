extends "res://script/turret/turret_gun.gd"

export(float) var offset = 10

func fire():
	var pos = get_global_pos()
	var t = get_global_transform()
	var dir = direction.rotated(t.get_rotation())

	_create_bullet(pos, dir)
	_create_bullet(pos, dir.rotated(deg2rad(abs(offset))))
	_create_bullet(pos, dir.rotated(deg2rad(-abs(offset))))

	reload_delay = reload
	_sfx_play()
