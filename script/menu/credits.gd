extends Control

func _ready():
	var rtl = get_node("vbc/rtl")
	rtl.set_scroll_active(false)
	var s = TS.translate("ID_CREDITS_MSG")
	var lines = s.split("|")
	rtl.newline()
	for l in lines:
		rtl.add_text(l)
		rtl.newline()

func _on_continue_released():
	get_node("../").back_dlg()
