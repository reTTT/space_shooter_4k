extends Node

func apply():
	var cfg = get_node("/root/config")
	if cfg == null:
		return
	
	var sound = cfg.get_value("options", "sound", true)
	var music = cfg.get_value("options", "music", true)
	
	get_node("vbc/sound").set_val(sound)
	get_node("vbc/music").set_val(music)
	
	AS.set_stream_global_volume_scale(music)
	AS.set_fx_global_volume_scale(sound)
	cfg.serialize()

func _on_visibility_changed():
	if  is_visible():
		apply()

func _ready():
	apply()
	connect("visibility_changed", self, "_on_visibility_changed")

func _on_sound_value_changed( value ):
	var cfg = get_node("/root/config")
	cfg.set_value("options", "sound", value)

func _on_music_value_changed( value ):
	var cfg = get_node("/root/config")
	cfg.set_value("options", "music", value)

func _on_apply_released():
	apply()
	get_node("../").back_dlg()
