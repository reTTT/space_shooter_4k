extends Control

func show():
	.show()
	_on_show()

func _on_show():
	var score = get_node("/root/game").score
	var score_lab = get_node("panel/vbc/score")
	score_lab.set_text(TS.translate("ID_SCORE") + ": " + str(score))

func _on_try_again_released():
	get_tree().call_group(SceneTree.GROUP_CALL_REALTIME, "player_spawn", "spawn")
	get_node("../").hide_dlg()

func _on_main_menu_released():
	get_node("/root/scene_mng").goto_scene("res://scene/mmenu.xscn")
