extends "res://script/menu/dlg_mng.gd"

var adbuddiz_count = 0
var game

func _ready():
	game = get_node("/root/game")
	set_process(true)

func _process(dt):
	if Input.is_key_pressed(KEY_M) && active == null:
		show_dlg("gmenu")

func _on_cheats_pressed():
	print("cheats press")

func _on_gmenu_released():
	show_dlg("gmenu")

func on_player_dead():
	show_dlg("game_over")

func on_game_over():
	adbuddiz_count += 1
	show_dlg("game_over")
	
	get_node("/root/chartboost").showInterstitial()
	
#	if !(adbuddiz_count % 2):
#		get_node("/root/chartboost").showInterstitial()
#	else:
#		get_node("/root/adbuddiz").show_ad()

func _set_pause():
	if game == null:
		return
	if active:
		game.set_state(game.S_PAUSE)
	else:
		game.set_state(game.S_PLAY)

func show_dlg(dlg_id):
	.show_dlg(dlg_id)
	_set_pause()

func back_dlg():
	.back_dlg()
	_set_pause()
	
func hide_dlg():
	.hide_dlg()
	_set_pause()

func _notification(what):
	if game && what == MainLoop.NOTIFICATION_WM_FOCUS_OUT && game.get_state() == game.S_PLAY:
		show_dlg("gmenu")