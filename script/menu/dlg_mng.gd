extends Node

export(bool) var paused = true
export var start_dlg_id = ""

var dlg_stack = []
var dlg_list = {}
var active = null

func show_dlg(dlg_id):
	if active != null:
		if active.get_name() == dlg_id:
			return
	
		dlg_stack.push_back(active)

	active = null
	var keys = dlg_list.keys()
	
	for key in keys:
		var dlg = dlg_list[key]
		
		if key == dlg_id:
			dlg.show()
			active = dlg
		else:
			dlg.hide()

	if paused:
		get_tree().set_pause(true)

func back_dlg():
	if active != null:
		active.hide()
		active = null

	if dlg_stack.empty():
		return
	
	var idx = dlg_stack.size()-1
	active = dlg_stack[idx]
	dlg_stack.remove(idx)
	
	var keys = dlg_list.keys()
	
	for key in keys:
		var dlg = dlg_list[key]
		
		if active == dlg:
			dlg.show()
		else:
			dlg.hide()
			
	if paused:
		get_tree().set_pause(true)

func hide_dlg():
	var keys = dlg_list.keys()
	for key in keys:
		var dlg = dlg_list[key]
		dlg.hide()
	
	if paused:
		get_tree().set_pause(false)

	active = null
	dlg_stack.clear()

func _ready():
	if !paused:
		get_tree().set_pause(false)

	for c in  get_children():
		dlg_list[c.get_name()] = c
	
	if !start_dlg_id.empty():
		show_dlg(start_dlg_id)
	else:
		hide_dlg()
