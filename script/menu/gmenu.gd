extends Control

func _on_continue_released():
	get_node("../").hide_dlg()

func _on_options_released():
	get_node("../").show_dlg("options")

func _on_mmenu_released():
	get_node("/root/scene_mng").goto_scene("res://scene/mmenu.xscn")
