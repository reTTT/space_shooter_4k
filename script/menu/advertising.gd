extends PopupPanel

var cb = null
var show_banner = false

var thread = Thread.new()
var loop = true

func get_delay_long():
	return randi() % (50 * 1000) + (10 * 1000)

func get_delay_far():
	return randi() % (4 * 1000) + (8 * 1000)

func banner_looper(arg):
	var last = OS.get_ticks_msec()
	var delay = get_delay_far()
	while loop:
		var delta = abs(OS. get_ticks_msec() - last)
		if delta > delay:
			last = OS. get_ticks_msec()
			if cb.isAnyViewVisible():
				banner_hide()
				delay = get_delay_long()
			else:
				banner_show()
				delay = get_delay_far()
				
	return null


func banner_hide():
	cb.closeImpression()
	print("hide banner")
	show_banner = false

func banner_show():
	cb.showInterstitial()
	print("show banner")
	show_banner = true

func _exit_tree():
	if thread.is_active():
		loop = false
		banner_hide()

func _on_chartboost_released():
	cb = get_node("/root/chartboost")
	if cb:
		if !thread.is_active():
			loop = true
			thread.start(self, "banner_looper", null)
		else:
			loop = false
			banner_hide()
			thread.wait_to_finish()


func _on_adbuddiz_released():
	var ad = get_node("/root/adbuddiz")
	ad.show_ad()
	

func _on_continue_released():
	get_node("../").back_dlg()
