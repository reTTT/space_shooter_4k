extends PopupPanel

func _on_play_released():
	get_node("/root/scene_mng").goto_scene("res://scene/game.xscn") 

func _on_options_released():
	get_node("../").show_dlg("options")

func _on_credits_released():
	get_node("../").show_dlg("credits") 
