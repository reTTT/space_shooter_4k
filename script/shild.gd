extends AnimatedSprite

var ship_immortal = false

func _set_color(color):
	set_modulate(color)
	get_node("light").set_color(color)

func _on_add_buff(buff):
	if buff.get_name() == "immortal":
		get_node("ani").stop()
		ship_immortal = true
		set_opacity(1)
		var count = get_sprite_frames().get_frame_count()
		set_frame(count-1)
		_set_color(Color(0, 1, 0, 1))

func _on_remove_buff(buff):
	if buff.get_name() == "immortal":
		ship_immortal = false
		set_opacity(0)
		_set_color(Color(1, 0, 0, 1))

func _on_damage():
	if get_parent().get_health_in_percent() == 0 || ship_immortal:
		return
	var count = get_sprite_frames().get_frame_count()
	set_frame( (count-1)*get_parent().get_health_in_percent() )
	get_node("ani").play("blink")

func _ready():
	set_opacity(0)
	get_parent().connect("damage", self, "_on_damage")
	get_parent().connect("add_buff", self, "_on_add_buff")
	get_parent().connect("remove_buff", self, "_on_remove_buff")
