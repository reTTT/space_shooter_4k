
var data = [
	{
		"name": "immortal",
		"icon": preload("res://texture/bonus/shield.png"),
		"speed": [100, 100],
		"score": 0,
		"period": 10,
		"script": preload("res://script/bonus/immortal.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
	{
		"name": "fire_rate",
		"icon": preload("res://texture/bonus/speed.png"),
		"speed": [100, 100],
		"score": 0,
		"period": 10,
		"reload": 0.25,
		"script": preload("res://script/bonus/fire_rate.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
	{
		"name": "health",
		"icon": preload("res://texture/bonus/health.png"),
		"single": true,
		"speed": [100, 100],
		"score": 0,
		"health": 1,
		"fully": false,
		"script": preload("res://script/bonus/health.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
	{
		"name": "upgrade_gun",
		"icon": preload("res://texture/bonus/upgrade_gun.png"),
		"single": true,
		"speed": [100, 100],
		"score": 0,
		"health": 1,
		"fully": false,
		"script": preload("res://script/bonus/upgrade_gun.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
	{
		"name": "rocket_red",
		"icon": preload("res://texture/bonus/rocket_red.png"),
		"speed": [100, 100],
		"score": 0,
		"period": 10,
		"script": preload("res://script/bonus/rocket_red.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
	{
		"name": "rocket_green",
		"icon": preload("res://texture/bonus/rocket_green.png"),
		"speed": [100, 100],
		"score": 0,
		"period": 10,
		"script": preload("res://script/bonus/rocket_green.gd"),
		"item": {
			"inst": preload("res://prefab/ui/buf_item_icon.xscn"),
		},
	},
]

func get_data():
	return data
