var data = [
	{
		"name": "big",
		"tiles": [
			"res://texture/meteors/meteor_1.png",
			"res://texture/meteors/meteor_2.png",
			"res://texture/meteors/meteor_3.png",
			"res://texture/meteors/meteor_4.png",
			"res://texture/meteors/meteor_5.png",
			"res://texture/meteors/meteor_6.png",
		],
		
		"radius": 46,
		"scale": [1.0, 1.5],
		"speed": [35, 50],
		"score": 20,
		"health": 5,
		"drop": "med",
	},
	{
		"name": "med",
		"tiles": [
			"res://texture/meteors/meteor_1.png",
			"res://texture/meteors/meteor_2.png",
			"res://texture/meteors/meteor_3.png",
			"res://texture/meteors/meteor_4.png",
			"res://texture/meteors/meteor_5.png",
			"res://texture/meteors/meteor_6.png",
		],
		
		"radius": 46,
		"scale": [0.75, 1.0],
		"speed": [45, 65],
		"score": 15,
		"health": 4,
	},
	{
		"name": "small",
		"tiles": [
			"res://texture/meteors/meteor_1.png",
			"res://texture/meteors/meteor_2.png",
			"res://texture/meteors/meteor_3.png",
			"res://texture/meteors/meteor_4.png",
			"res://texture/meteors/meteor_5.png",
			"res://texture/meteors/meteor_6.png",
		],
		
		"radius": 46,
		"scale": [0.5, 0.75],
		"speed": [40, 70],
		"score": 10,
		"health": 1,
	},
	{
		"name": "tiny",
		"tiles": [
			"res://texture/meteors/meteor_1.png",
			"res://texture/meteors/meteor_2.png",
			"res://texture/meteors/meteor_3.png",
			"res://texture/meteors/meteor_4.png",
			"res://texture/meteors/meteor_5.png",
			"res://texture/meteors/meteor_6.png",
		],
		"radius": 46,
		"scale": [0.25, 0.5],
		"speed": [40, 80],
		"score": 5,
		"health": 1,
	},
]

func get_data():
	return data
