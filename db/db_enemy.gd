var data = [
	{
		"name": "easy",
		"speed": [400, 450],
		"score": 5,
		"health": 1,
		"weapon": "turret_1",
	},
	{
		"name": "medium",
		"speed": [350, 400],
		"score": 10,
		"health": 2,
		"weapon": "turret_2",
	},
	{
		"name": "hard",
		"speed": [300, 350],
		"score": 15,
		"health": 3,
		"weapon": "turret_3",
	},
]

func get_data():
	return data
