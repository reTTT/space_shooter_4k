var data = [
	{
		"name": "turret_1",
		"chance": 0.1,
		"delta" : 20,
		"reload": 0.5,
		"fire_delay": [0.2, 0.4],
		"damage": 1,
		"bullet": "res://prefab/bullet/bullet_blue.xscn",
	},
	{
		"name": "turret_2",
		"chance": 0.25,
		"delta" : 70,
		"reload": 1.0,
		"fire_delay": [0.3, 0.8],
		"damage": 2,
		"bullet": "res://prefab/bullet/bullet_blue.xscn",
	},
	{
		"name": "turret_3",
		"chance": 1.0,
		"delta" : 250,
		"reload": 1.5,
		"fire_delay": [0.4, 0.9],
		"damage": 3,
		"bullet": "res://prefab/bullet/bullet_blue.xscn",
	},
]

func get_data():
	return data
